package Mesh;

import Geometry.Lines.Line3d;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MeshLineTest {

	private final Point3d P1 = new Point3d(11,12,13);
	private final Point3d P2 = new Point3d(21,22,23);

	private final Vector3d NORMAL1 = new Vector3d(101,102,103);
	private final Vector3d NORMAL2 = new Vector3d(201,202,203);

	@Test
	void endpointConstructor() {

		IMeshLine line = new MeshLine(P1,P2);
		assertEquals(P1, line.getP1());
		assertEquals(P2, line.getP2());
		assertListsEqual(new ArrayList<>(), line.getNormals());
		assertFalse(line.isFixed());
	}

	@Test
	void lineConstructor() {

		IMeshLine line = new MeshLine(new Line3d(P1,P2));
		assertEquals(P1, line.getP1());
		assertEquals(P2, line.getP2());
		assertListsEqual(new ArrayList<>(), line.getNormals());
		assertFalse(line.isFixed());
	}

	@Test
	void valuesConstructor() {

		List<Vector3d> normals = Arrays.asList(NORMAL1, NORMAL2);
		boolean isFixed = true;
		IMeshLine line = new MeshLine(P1,P2,normals,isFixed);
		assertEquals(P1, line.getP1());
		assertEquals(P2, line.getP2());
		assertListsEqual(normals, line.getNormals());
		assertEquals(isFixed, line.isFixed());
	}

	@Test
	void copyConstructor() {

		List<Vector3d> normals = Arrays.asList(NORMAL1, NORMAL2);
		boolean isFixed = true;
		IMeshLine line = new MeshLine(new MeshLine(P1,P2,normals,isFixed));
		assertEquals(P1, line.getP1());
		assertEquals(P2, line.getP2());
		assertListsEqual(normals, line.getNormals());
		assertEquals(isFixed, line.isFixed());
	}

	@Test
	void getNormals() {

		List<List<Vector3d>> normalsList = new ArrayList<>();
		normalsList.add(new ArrayList<>());
		normalsList.add(Arrays.asList(NORMAL1));
		normalsList.add(Arrays.asList(NORMAL1, NORMAL2));
		normalsList.add(Arrays.asList(NORMAL1, NORMAL2, NORMAL1));

		for(List<Vector3d> normals : normalsList) {
			IMeshLine line = new MeshLine(new MeshLine(P1, P2, normals, true));
			assertListsEqual(normals, line.getNormals());
		}
	}

	@Test
	void isFixed() {

		List<Vector3d> normals = Arrays.asList(NORMAL1, NORMAL2);
		for(boolean isFixed : new boolean[] {true, false}) {
			IMeshLine line = new MeshLine(P1, P2, normals, isFixed);
			assertEquals(isFixed, line.isFixed());
		}
	}

	@Test
	void transform_fixed() {

		List<Vector3d> normals = Arrays.asList(NORMAL1, NORMAL2);
		IMeshLine line = new MeshLine(P1,P2,normals,true);

		Matrix4d flipY = new Matrix4d(
			1,0,0,0,
			0,-1,0,0,
			0,0,1,0,
			0,0,0,1
		);
		line.transform(flipY);

		assertEquals(new Point3d(11,-12,13), line.getP1());
		assertEquals(new Point3d(21,-22,23), line.getP2());

		assertEquals(2, normals.size());
		assertEquals(NORMAL1, normals.get(0));
		assertEquals(NORMAL2, normals.get(1));
	}

	@Test
	void transform_notFixed() {

		List<Vector3d> normals = Arrays.asList(NORMAL1, NORMAL2);
		IMeshLine line = new MeshLine(P1,P2,normals,false);

		Matrix4d flipY = new Matrix4d(
			1,0,0,0,
			0,-1,0,0,
			0,0,1,0,
			0,0,0,1
		);
		line.transform(flipY);

		assertEquals(new Point3d(11,-12,13), line.getP1());
		assertEquals(new Point3d(21,-22,23), line.getP2());

		assertEquals(2, normals.size());
		assertEquals(new Vector3d(101,-102,103), normals.get(0));
		assertEquals(new Vector3d(201,-202,203), normals.get(1));
	}

	@Test
	void equals() {

		List<Point3d[]> endpointsList = new ArrayList<>();
		endpointsList.add(new Point3d[] {P1,P2});
		endpointsList.add(new Point3d[] {P1,P1});

		List<List<Vector3d>> normalsList = new ArrayList<>();
		normalsList.add(new ArrayList<>());
		normalsList.add(Arrays.asList(NORMAL1));
		normalsList.add(Arrays.asList(NORMAL1, NORMAL2));
		normalsList.add(Arrays.asList(NORMAL1, NORMAL2, NORMAL1));

		List<Boolean> isFixedList = new ArrayList<>();
		isFixedList.add(true);
		isFixedList.add(false);

		IMeshLine line00 = new MeshLine(
			endpointsList.get(0)[0],
			endpointsList.get(0)[1],
			normalsList.get(2),
			isFixedList.get(0)
		);

		for(int i=0; i<endpointsList.size(); i++) {
			for(int j=0; j<normalsList.size(); j++) {
				for(int k=0; k<isFixedList.size(); k++) {
					IMeshLine other = new MeshLine(
						endpointsList.get(i)[0],
						endpointsList.get(i)[1],
						normalsList.get(j),
						isFixedList.get(k)
					);
					if(i==0 && j==2 && k==0) assertEquals(line00, other);
					else assertNotEquals(line00, other);
				}
			}
		}
	}

	private void assertListsEqual(List<Vector3d> list1, List<Vector3d> list2) {

		Vector3d[] array1 = list1.toArray(new Vector3d[list1.size()]);
		Vector3d[] array2 = list2.toArray(new Vector3d[list2.size()]);
		assertArrayEquals(array1, array2);
	}
}