package Mesh;

import Geometry.Lines.ILine3d;
import Geometry.Lines.Line3d;
import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.Polygon3d;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.awt.*;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class MeshTest {

	private final Point3d p1 = new Point3d(11,12,13);
	private final Point3d p2 = new Point3d(21,22,23);
	private final Point3d p3 = new Point3d(31,32,33);
	private final Point3d p4 = new Point3d(41,42,43);
	private final Point3d p5 = new Point3d(51,52,53);
	private final Point3d p6 = new Point3d(61,62,63);

	@Test
	void defaultConstructor() {
		IMesh mesh = new Mesh();
		assertArrayEquals(new ILine3d[0], mesh.getEdges());
		assertArrayEquals(new IPolygon3d[0], mesh.getFaces());
		assertArrayEquals(new Color[0], mesh.getFaceColours());
		assertNull(mesh.getCentre());
	}

	@Test
	void meshArgsConstructor() {

		IMeshLine line1 = new MeshLine(
			p1,
			p2,
			Arrays.asList(new Vector3d(1,2,3), new Vector3d(4,5,6)),
			true
		);
		IMeshLine line2 = new MeshLine(
			p3,
			p4,
			Arrays.asList(new Vector3d(2,3,4), null),
			false
		);

		IMeshPolygon polygon1 = new MeshPolygon(
			new Point3d[] {p1,p2,p3,p4},
			new Vector3d(7,8,9),
			false,
			Color.red
		);

		IMeshPolygon polygon2 = new MeshPolygon(
			new Point3d[] {p3,p4,p1,p2},
			null,
			true,
			null
		);

		IMeshLine[] lines = {line1, line2};
		IMeshPolygon[] polygons = {polygon1, polygon2};

		IMesh mesh = new Mesh(lines, polygons);

		assertArrayEquals(lines, mesh.getEdges());
		assertArrayEquals(polygons, mesh.getFaces());
		assertEquals(new Point3d(26, 27, 28), mesh.getCentre());
		assertArrayEquals(new Color[] {Color.red, null}, mesh.getFaceColours());
	}

	@Test
	void edgeFaceConstructor() {
		ILine3d[] edges = {
			new Line3d(p1, p2),
			new Line3d(p4, p6)
		};
		IPolygon3d[] faces = {
			new Polygon3d(p3, p2, p5),
			new Polygon3d(p1, p4, p2, p3)
		};
		IMesh mesh = new Mesh(edges, faces);

		assertArrayEquals(edges, mesh.getEdges());
		assertArrayEquals(faces, mesh.getFaces());
		assertArrayEquals(new Color[faces.length], mesh.getFaceColours());
		assertEquals(new Point3d(36,37,38), mesh.getCentre());
	}
	@Test
	void valuesConstructor() {
		ILine3d[] edges = {
			new Line3d(p1, p2),
			new Line3d(p4, p6)
		};
		IPolygon3d[] faces = {
			new Polygon3d(p3, p2, p5),
			new Polygon3d(p1, p4, p2, p3)
		};
		Color[] faceColours = {
			new Color(1,2,3),
			new Color(4,5,6,7)
		};
		IMesh mesh = new Mesh(edges, faces, faceColours);

		assertArrayEquals(edges, mesh.getEdges());
		assertArrayEquals(faces, mesh.getFaces());
		assertArrayEquals(faceColours, mesh.getFaceColours());
		assertEquals(new Point3d(36,37,38), mesh.getCentre());
	}

	@Test
	void valuesConstructor_invalidFaces() {
		ILine3d[] edges = {
			new Line3d(p1, p2),
			new Line3d(p4, p6)
		};
		IPolygon3d[] faces = {
			new Polygon3d(p3, p2, p5),
			new Polygon3d(p1, p4)
		};
		Color[] faceColours = {
			new Color(1,2,3),
			new Color(4,5,6,7)
		};
		assertThrows(RuntimeException.class, () -> new Mesh(edges, faces, faceColours));
	}

	@Test
	void copyConstructor() {
		ILine3d[] edges = {
			new Line3d(p1, p2),
			new Line3d(p4, p6)
		};
		IPolygon3d[] faces = {
			new Polygon3d(p3, p2, p5),
			new Polygon3d(p1, p4, p2, p3)
		};
		Color[] faceColours = {
			new Color(1,2,3),
			new Color(4,5,6,7)
		};
		IMesh mesh1 = new Mesh(edges, faces, faceColours);
		IMesh mesh2 = new Mesh(mesh1);

		assertArrayEquals(edges, mesh2.getEdges());
		assertArrayEquals(faces, mesh2.getFaces());
		assertArrayEquals(faceColours, mesh2.getFaceColours());
		assertEquals(mesh1.getCentre(), mesh2.getCentre());
	}

	@Test
	void mergeConstructor() {
		ILine3d[] edges1 = { new Line3d(p1, p2), new Line3d(p3, p4) };
		ILine3d[] edges2 = { new Line3d(p5, p6) };
		ILine3d[] edges3 = null;
		ILine3d[] edges = {
			new Line3d(p1, p2),
			new Line3d(p3, p4),
			new Line3d(p5, p6),
		};

		IPolygon3d[] faces1 = {	new Polygon3d(p1, p2, p3) };
		IPolygon3d[] faces2 = {	new Polygon3d(p2, p3, p4, p5) };
		IPolygon3d[] faces3 = {
			new Polygon3d(p3, p4, p5, p1, p2),
			new Polygon3d(p4, p5, p6)
		};
		IPolygon3d[] faces = {
			new Polygon3d(p1, p2, p3),
			new Polygon3d(p2, p3, p4, p5),
			new Polygon3d(p3, p4, p5, p1, p2),
			new Polygon3d(p4, p5, p6)
		};

		Color[] faceColours1 = { new Color(1,2,3) };
		Color[] faceColours2 = { new Color(4, 5, 6, 7) };
		Color[] faceColours3 = null;
		Color[] faceColours = {
			new Color(1,2,3),
			new Color(4, 5, 6, 7),
			null,
			null
		};

		IMesh mesh1 = new Mesh(edges1, faces1, faceColours1);
		IMesh mesh2 = new Mesh(edges2, faces2, faceColours2);
		IMesh mesh3 = new Mesh(edges3, faces3, faceColours3);
		IMesh mesh = new Mesh(new IMesh[] { mesh1, mesh2, mesh3 });

		assertArrayEquals(edges, mesh.getEdges());
		assertArrayEquals(faces, mesh.getFaces());
		assertArrayEquals(faceColours, mesh.getFaceColours());
	}

	@Test
	void getEdges() {
		ILine3d[] edges = {};
		IPolygon3d[] faces = {
			new Polygon3d(p3, p2, p5),
			new Polygon3d(p1, p4, p2, p3)
		};
		IMesh mesh = new Mesh(edges, faces, null);
		assertArrayEquals(edges, mesh.getEdges());
	}

	@Test
	void getFaces() {
		ILine3d[] edges = {};
		IPolygon3d[] faces = {
			new Polygon3d(p1, p2, p3),
			new Polygon3d(p2, p3, p4, p6),
			new Polygon3d(p3, p4, p5)
		};
		IMesh mesh = new Mesh(edges, faces, null);
		assertArrayEquals(faces, mesh.getFaces());
	}

	@Test
	void getFaceColours_nullCase() {
		ILine3d[] edges = {};
		IPolygon3d[] faces = {
			new Polygon3d(p1, p2, p3),
			new Polygon3d(p2, p3, p4, p6),
			new Polygon3d(p3, p4, p5)
		};
		Color[] faceColours = {null, null, null};
		IMesh mesh = new Mesh(edges, faces, faceColours);
		assertArrayEquals(faceColours, mesh.getFaceColours());
	}

	@Test
	void getFaceColours() {
		ILine3d[] edges = {};
		IPolygon3d[] faces = {
			new Polygon3d(p1, p2, p3),
			new Polygon3d(p2, p3, p4, p6),
			new Polygon3d(p3, p4, p5)
		};
		Color[] faceColours = {
			new Color(1,2,3),
			new Color(4,5,6,7),
			null
		};
		IMesh mesh = new Mesh(edges, faces, faceColours);
		assertArrayEquals(faceColours, mesh.getFaceColours());
	}

	@Test
	void getCentre() {
		ILine3d[] edges = {
			new Line3d(p1, p2),
			new Line3d(p4, p6)
		};
		IPolygon3d[] faces = {
			new Polygon3d(p3, p2, p5),
			new Polygon3d(p1, p4, p2, p3)
		};
		Point3d expected = new Point3d(36,37,38);

		IMesh mesh = new Mesh(edges, faces, null);
		assertEquals(expected, mesh.getCentre());
	}

	@Test
	void transform() {

		Vector3d v1 = new Vector3d(101,102,103);
		Vector3d v2 = new Vector3d(201,202,203);
		IMeshLine[] edges = {
			new MeshLine(p1, p2, Arrays.asList(v1,v2), true),
			new MeshLine(p1, p2, Arrays.asList(v1,v2), false)
		};
		IPolygon3d[] faces = {
			new MeshPolygon(new Point3d[] {p1, p2, p3}, v1, true, Color.red),
			new MeshPolygon(new Point3d[] {p1, p2, p3}, v1, false, Color.red),
		};
		Matrix4d flipY = new Matrix4d(
			1,0,0,0,
			0,-1,0,0,
			0,0,1,0,
			0,0,0,1
		);
		IMesh actual = new Mesh(edges, faces);
		actual.transform(flipY);

		Point3d q1 = new Point3d(11,-12,13);
		Point3d q2 = new Point3d(21,-22,23);
		Point3d q3 = new Point3d(31,-32,33);
		Vector3d w1 = new Vector3d(101,-102,103);
		Vector3d w2 = new Vector3d(201,-202,203);

		IMeshLine[] expectedEdges = {
			new MeshLine(q1, q2, Arrays.asList(v1,v2), true),
			new MeshLine(q1, q2, Arrays.asList(w1,w2), false)
		};
		IPolygon3d[] expectedFaces = {
			new MeshPolygon(new Point3d[] {q1, q2, q3}, v1, true, Color.red),
			new MeshPolygon(new Point3d[] {q1, q2, q3}, w1, false, Color.red),
		};
		IMesh expected = new Mesh(expectedEdges, expectedFaces);

		assertEquals(expected, actual);
	}

	@Test
	void equals() {

		// create different sets of edges
		ILine3d[] edges1 = null;
		ILine3d[] edges2 = {};
		ILine3d[] edges3 = { new Line3d(p1, p2) };
		ILine3d[] edges4 = { new Line3d(p1, p2), new Line3d(p3, p4) };
		ILine3d[] edges5 = { new Line3d(p1, p2), new Line3d(p3, p3) };
		ILine3d[][] allEdges = {edges1, edges2, edges3, edges4, edges5};

		// create different sets of faces
		IPolygon3d[] faces1 = null;
		IPolygon3d[] faces2 = {};
		IPolygon3d[] faces3 = {
			new Polygon3d(p1, p2, p3)
		};
		IPolygon3d[] faces4 = {
			new Polygon3d(p1, p2, p3, p4)
		};
		IPolygon3d[] faces5 = {
			new Polygon3d(p1, p2, p3),
			new Polygon3d(p4, p5, p5)
		};
		IPolygon3d[][] allFaces = {faces1, faces2, faces3, faces4, faces5};

		// create face colours
		Color[] faceColours1 = null;
		Color[] faceColours2 = null;
		Color[] faceColours3 = { new Color(1,2,3) };
		Color[] faceColours4 = { new Color(4,5,6), null };
		Color[] faceColours5 = { null, new Color(7,8,9,10) };
		Color[][] faceColours = { faceColours1, faceColours2, faceColours3, faceColours4, faceColours5 };

		// create different meshes with various edges and faces
		HashSet<int[]> skipped = new HashSet<>();

		IMesh[][][] meshes = new IMesh[5][5][5];
		for(int i=0; i<5; i++) {
			for(int j=0; j<5; j++) {
				for(int k=0; k<5; k++) {
					try {
						meshes[i][j][k] = new Mesh(allEdges[i], allFaces[j], faceColours[k]);
					}
					catch(Exception e) {
						int size1 = allFaces[j]==null ? 0 : allFaces[j].length;
						int size2 = faceColours[k]==null ? 0 : faceColours[k].length;
						assertNotEquals(size1, size2);
						skipped.add(new int[] {i,j,k});
						return;
					}
				}
			}
		}

		// test equals method
		IMesh mesh444 = new Mesh(allEdges[4], allFaces[4], faceColours[4]);
		for(int i=0; i<5; i++) {
			for(int j=0; j<5; j++) {
				for(int k=0; k<5; k++) {
					if(skipped.contains(new int[] {i,j,k})) continue;
					if (i == 4 && j == 4 && k==4) assertEquals(meshes[i][j][k], mesh444);
					else assertNotEquals(meshes[i][j][k], mesh444);
				}
			}
		}
	}

	@Test
	void equals_colorCompare() {

		// create different sets of edges
		ILine3d[] edges = { new Line3d(p1, p2), new Line3d(p3, p4) };

		// create different sets of faces
		IPolygon3d[] faces = {
			new Polygon3d(p1, p2, p3),
			new Polygon3d(p4, p5, p6)
		};

		assertEquals(
			new Mesh(edges, faces, null),
			new Mesh(edges, faces, null)
		);

		assertNotEquals(
			new Mesh(edges, faces, null),
			new Mesh(edges, faces, new Color[] { new Color(4,5,6), null })
		);

		assertEquals(
			new Mesh(edges, faces, new Color[] { new Color(4,5,6), null }),
			new Mesh(edges, faces, new Color[] { new Color(4,5,6), null })
		);

		assertNotEquals(
			new Mesh(edges, faces, new Color[] { new Color(4,5,6), new Color(1,2,3) }),
			new Mesh(edges, faces, new Color[] { new Color(4,5,6), null })
		);
	}
}