package Mesh;

import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MeshPolygonTest {

	private final Point3d P1 = new Point3d(11,12,13);
	private final Point3d P2 = new Point3d(21,22,23);
	private final Point3d P3 = new Point3d(31,32,33);
	private final Point3d P4 = new Point3d(41,42,43);

	private final Vector3d NORMAL1 = new Vector3d(101,102,103);
	private final Vector3d NORMAL2 = new Vector3d(201,202,203);

	@Test
	void verticesConstructor() {

		IMeshPolygon polygon = new MeshPolygon(P1,P2,P3,P4);
		assertArrayEquals(new Point3d[] {P1,P2,P3,P4}, polygon.getVertices());
		assertNull(polygon.getNormal());
		assertFalse(polygon.isFixed());
	}

	@Test
	void valuesConstructor() {

		Point3d[] vertices = new Point3d[] {P1,P2,P3,P4};
		Vector3d normal = NORMAL1;
		boolean isFixed = true;
		Color faceColour = Color.red;
		IMeshPolygon polygon = new MeshPolygon(vertices,normal,isFixed,faceColour);

		assertArrayEquals(vertices, polygon.getVertices());
		assertEquals(normal, polygon.getNormal());
		assertEquals(isFixed, polygon.isFixed());
		assertEquals(faceColour, polygon.getColor());
	}

	@Test
	void verticesColourConstructor() {

		Point3d[] vertices = new Point3d[] {P1,P2,P3,P4};
		Color faceColour = Color.red;
		IMeshPolygon polygon = new MeshPolygon(vertices,faceColour);

		assertArrayEquals(vertices, polygon.getVertices());
		assertNull(polygon.getNormal());
		assertFalse(polygon.isFixed());
		assertEquals(faceColour, polygon.getColor());
	}

	@Test
	void copyConstructor() {

		Point3d[] vertices = new Point3d[] {P1,P2,P3,P4};
		Vector3d normal = NORMAL1;
		boolean isFixed = true;
		Color faceColour = Color.red;
		IMeshPolygon polygon = new MeshPolygon(new MeshPolygon(vertices,normal,isFixed,faceColour));

		assertArrayEquals(vertices, polygon.getVertices());
		assertEquals(normal, polygon.getNormal());
		assertEquals(isFixed, polygon.isFixed());
		assertEquals(faceColour, polygon.getColor());
	}

	@Test
	void getNormal() {

		Point3d[] vertices = new Point3d[] {P1,P2,P3,P4};
		boolean isFixed = true;
		Color faceColour = Color.red;

		IMeshPolygon polygon1 = new MeshPolygon(vertices,null,isFixed,faceColour);
		assertNull(polygon1.getNormal());

		IMeshPolygon polygon2 = new MeshPolygon(vertices,NORMAL1,isFixed,faceColour);
		assertEquals(NORMAL1, polygon2.getNormal());
	}

	@Test
	void isFixed() {
		Point3d[] vertices = new Point3d[] {P1,P2,P3,P4};
		Vector3d normal = NORMAL1;
		Color faceColour = Color.red;
		for(boolean isFixed : new boolean[] {true, false}) {
			IMeshPolygon polygon = new MeshPolygon(vertices,normal,isFixed,faceColour);
			assertEquals(isFixed, polygon.isFixed());
		}
	}

	@Test
	void getColor() {
		Point3d[] vertices = new Point3d[] {P1,P2,P3,P4};
		Vector3d normal = NORMAL1;
		boolean isFixed = true;
		for(Color faceColour : new Color[] {null, Color.red, Color.blue}) {
			IMeshPolygon polygon = new MeshPolygon(vertices,normal,isFixed,faceColour);
			assertEquals(faceColour, polygon.getColor());
		}
	}

	@Test
	void transform_fixed() {

		Point3d[] vertices = new Point3d[] {P1,P2,P3,P4};
		IMeshPolygon polygon = new MeshPolygon(vertices, NORMAL1, true, Color.red);

		Matrix4d flipY = new Matrix4d(
			1,0,0,0,
			0,-1,0,0,
			0,0,1,0,
			0,0,0,1
		);
		polygon.transform(flipY);

		assertArrayEquals(
			new Point3d[] {
				new Point3d(11,-12,13),
				new Point3d(21,-22,23),
				new Point3d(31,-32,33),
				new Point3d(41,-42,43)
			},
			polygon.getVertices()
		);

		assertEquals(new Vector3d(101,102,103), polygon.getNormal());
		assertEquals(Color.red, polygon.getColor());
	}

	@Test
	void transform_notFixed() {

		Point3d[] vertices = new Point3d[] {P1,P2,P3,P4};
		IMeshPolygon polygon = new MeshPolygon(vertices, NORMAL1, false, Color.red);

		Matrix4d flipY = new Matrix4d(
			1,0,0,0,
			0,-1,0,0,
			0,0,1,0,
			0,0,0,1
		);
		polygon.transform(flipY);

		assertArrayEquals(
			new Point3d[] {
				new Point3d(11,-12,13),
				new Point3d(21,-22,23),
				new Point3d(31,-32,33),
				new Point3d(41,-42,43)
			},
			polygon.getVertices()
		);

		assertEquals(new Vector3d(101,-102,103), polygon.getNormal());
		assertEquals(Color.red, polygon.getColor());
	}

	@Test
	void equals() {

		List<Point3d[]> verticesList = new ArrayList<>();
		verticesList.add(null);
		verticesList.add(new Point3d[] {P1,P2,P3});
		verticesList.add(new Point3d[] {P1,P2,P4});

		List<Vector3d> normalList = new ArrayList<>();
		normalList.add(null);
		normalList.add(NORMAL1);
		normalList.add(NORMAL2);

		List<Boolean> isFixedList = new ArrayList<>();
		isFixedList.add(true);
		isFixedList.add(false);

		List<Color> coloursList = new ArrayList<>();
		coloursList.add(null);
		coloursList.add(Color.red);
		coloursList.add(Color.blue);

		IMeshPolygon polygon00 = new MeshPolygon(
			verticesList.get(0),
			normalList.get(1),
			isFixedList.get(0),
			coloursList.get(1)
		);

		for(int i=0; i<verticesList.size(); i++) {
			for(int j=0; j<normalList.size(); j++) {
				for(int k=0; k<isFixedList.size(); k++) {
					for(int l=0; l<coloursList.size(); l++) {
						IMeshPolygon other = new MeshPolygon(
							verticesList.get(i),
							normalList.get(j),
							isFixedList.get(k),
							coloursList.get(l)
						);
						if (i == 0 && j == 1 && k == 0 && l == 1) assertEquals(polygon00, other);
						else assertNotEquals(polygon00, other);
					}
				}
			}
		}
	}

	@Test
	void equals_compareToNulls() {

		List<Point3d[]> verticesList = new ArrayList<>();
		verticesList.add(null);
		verticesList.add(new Point3d[] {P1,P2,P3});
		verticesList.add(new Point3d[] {P1,P2,P4});

		List<Vector3d> normalList = new ArrayList<>();
		normalList.add(null);
		normalList.add(NORMAL1);
		normalList.add(NORMAL2);

		List<Boolean> isFixedList = new ArrayList<>();
		isFixedList.add(true);
		isFixedList.add(false);

		List<Color> coloursList = new ArrayList<>();
		coloursList.add(null);
		coloursList.add(Color.red);
		coloursList.add(Color.blue);

		IMeshPolygon polygon00 = new MeshPolygon(
			verticesList.get(0),
			normalList.get(0),
			isFixedList.get(0),
			coloursList.get(0)
		);

		for(int i=0; i<verticesList.size(); i++) {
			for(int j=0; j<normalList.size(); j++) {
				for(int k=0; k<isFixedList.size(); k++) {
					for(int l=0; l<coloursList.size(); l++) {
						IMeshPolygon other = new MeshPolygon(
							verticesList.get(i),
							normalList.get(j),
							isFixedList.get(k),
							coloursList.get(l)
						);
						if (i == 0 && j == 0 && k == 0 && l == 0) assertEquals(polygon00, other);
						else assertNotEquals(polygon00, other);
					}
				}
			}
		}
	}
}