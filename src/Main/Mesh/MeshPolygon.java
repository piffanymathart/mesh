package Mesh;

import Geometry.Polygons.Polygon3d;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.awt.*;

/**
 * Polygons used in the Mesh object.
 */
public class MeshPolygon extends Polygon3d implements IMeshPolygon {

	/**
	 * Outward normal vectors associated with the polygon.
	 */
	private Vector3d m_normal;

	/**
	 * Whether the normals are fixed or subject to transformations.
	 */
	private boolean m_fixed;

	/**
	 * Polygon colour (possibly null).
	 */
	private Color m_color;

	/**
	 * Constructor from polygon vertices.
	 * @param vertices Polygon vertices.
	 */
	public MeshPolygon(Point3d... vertices) {
		super(vertices);
		m_normal = null;
		m_fixed = false;
		m_color = null;
	}

	/**
	 * Constructor from polygon vertices, normal vector, fixed value, and colour.
	 * @param vertices Polygon vertices.
	 * @param normal Normal vector.
	 * @param fixed Whether normals are fixed or subject to transformations.
	 * @param color Polygon colour.
	 */
	public MeshPolygon(Point3d[] vertices, Vector3d normal, boolean fixed, Color color) {
		super(vertices);
		m_normal = deepCopy(normal);
		m_fixed = fixed;
		m_color = deepCopy(color);
	}

	/**
	 * Constructor from polygon vertices, normal vector, fixed value, and colour.
	 * @param vertices Polygon vertices.
	 * @param color Polygon colour.
	 */
	public MeshPolygon(Point3d[] vertices, Color color) {
		super(vertices);
		m_normal = null;
		m_fixed = false;
		m_color = deepCopy(color);
	}

	/**
	 * Copy constructor.
	 * @param polygon Polygon to copy from.
	 */
	public MeshPolygon(IMeshPolygon polygon) {
		super(polygon);
		m_normal = polygon.getNormal();
		m_fixed = polygon.isFixed();
		m_color = polygon.getColor();
	}

	/**
	 * Gets the outward normal vector of the polygon.
	 * @return outward normal vectors
	 */
	public Vector3d getNormal() {
		return deepCopy(m_normal);
	}

	/**
	 * Whether the normals are fixed or subject to transformations.
	 * @return Whether normals are fixed.
	 */
	public boolean isFixed() {
		return m_fixed;
	}

	/**
	 * Applies a 4x4 affine transformation.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	public void transform(Matrix4d matrix) {

		for(int i=0; i<m_vertices.length; i++) {
			Point3d p = m_vertices[i];
			matrix.transform(p);
			m_vertices[i] = p;
		}

		if(!m_fixed && m_normal!=null) {
			matrix.transform(m_normal);
		}
	}

	/**
	 * Gets the polygon colour.
	 * @return The polygon colour.
	 */
	public Color getColor() {
		return deepCopy(m_color);
	}

	/**
	 * Checks if the object is equal to the polygon.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the polygon.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!IMeshPolygon.class.isAssignableFrom(obj.getClass())) return false;
		return equals((IMeshPolygon) obj);
	}

	/**
	 * Checks if the two polygons are equal.
	 * @param polygon The polygon with which to compare.
	 * @return Whether the polygons are equal.
	 */
	private boolean equals(IMeshPolygon polygon) {
		if(!super.equals(polygon)) return false;

		if(m_normal==null) {
			if(polygon.getNormal()!=null) return false;
		}
		else {
			if(!m_normal.equals(polygon.getNormal())) return false;
		}

		if(m_fixed != polygon.isFixed()) return false;

		if(m_color==null) {
			if(polygon.getColor()!=null) return false;
		}
		else {
			if(!m_color.equals(polygon.getColor())) return false;
		}

		return true;
	}

	/**
	 * Makes a deep copy of the colour.
	 * @param c The colour to copy from.
	 * @return A copy of the colour.
	 */
	private Color deepCopy(Color c) {
		if(c == null) return null;
		return new Color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
	}

	/**
	 * Makes a deep copy of the vector.
	 * @param v The vector to copy from.
	 * @return A copy of the vector.
	 */
	private Vector3d deepCopy(Vector3d v) {
		return v==null ? null : new Vector3d(v);
	}
}
