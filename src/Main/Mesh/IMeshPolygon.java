package Mesh;

import Geometry.Polygons.IPolygon3d;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;
import java.awt.*;

/**
 * Polygons used in the Mesh object.
 */
public interface IMeshPolygon extends IPolygon3d {

	/**
	 * Gets the outward normal vector of the polygon.
	 * @return outward normal vectors
	 */
	Vector3d getNormal();

	/**
	 * Whether the normals are fixed or subject to transformations.
	 * @return Whether normals are fixed.
	 */
	boolean isFixed();

	/**
	 * Applies a 4x4 affine transformation.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	void transform(Matrix4d matrix);

	/**
	 * Gets the polygon colour.
	 * @return The polygon colour.
	 */
	Color getColor();
}
