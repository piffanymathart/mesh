package Mesh;

import Geometry.Lines.ILine3d;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;
import java.util.List;

/**
 * Lines used in the Mesh object.
 */
public interface IMeshLine extends ILine3d {

	/**
	 * Gets the outward normal vectors of the line.
	 * @return outward normal vectors.
	 */
	List<Vector3d> getNormals();

	/**
	 * Whether the normals are fixed or subject to transformations.
	 * @return Whether normals are fixed.
	 */
	boolean isFixed();

	/**
	 * Applies a 4x4 affine transformation.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	void transform(Matrix4d matrix);

	/**
	 * Checks if the object is equal to the line.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the line.
	 */
	@Override
	boolean equals(Object obj);
}
