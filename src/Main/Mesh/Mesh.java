package Mesh;

import Geometry.Lines.ILine3d;
import Geometry.Polygons.IPolygon3d;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A set of edges and faces representing a 3D object.
 */
public class Mesh implements IMesh {

	/**
	 * The set of edges.
	 */
	protected IMeshLine[] m_edges;

	/**
	 * The set of polygonal faces.
	 */
	protected IMeshPolygon[] m_faces;

	/**
	 * The centre.
	 */
	protected Point3d m_centre;

	/**
	 * Default constructor with no edges or faces.
	 */
	public Mesh() {
		set(new IMeshLine[0], new IMeshPolygon[0]);
	}

	/**
	 * Constructs a mesh object from an array of mesh lines and mesh polygons.
	 * @param edges The mesh lines.
	 * @param faces The mesh polygons.
	 */
	public Mesh(IMeshLine[] edges, IMeshPolygon[] faces) {
		set(edges, faces);
	}

	/**
	 * Constructs a mesh object from an array of edges and faces.
	 * @param edges The lines representing the edges.
	 * @param faces The polygons representing the faces.
	 */
	public Mesh(ILine3d[] edges, IPolygon3d[] faces) {
		set(toMeshLines(edges), toMeshPolygons(faces, null));
	}

	/**
	 * Constructs a mesh object from an array of edges, faces, and face colours.
	 * @param edges The lines representing the edges.
	 * @param faces The polygons representing the faces.
	 * @param faceColours The face colours.
	 */
	public Mesh(ILine3d[] edges, IPolygon3d[] faces, Color[] faceColours) {
		set(toMeshLines(edges), toMeshPolygons(faces, faceColours));
	}

	/**
	 * Copy constructor.
	 * @param mesh The object to copy from.
	 */
	public Mesh(IMesh mesh) {
		set(mesh.getEdges(), mesh.getFaces());
	}

	/**
	 * Constructs a mesh that combines the input meshes.
	 * @param meshes The meshes to combine.
	 */
	public Mesh(IMesh[] meshes) {
		List<IMeshLine> allEdges = new ArrayList<>();
		List<IMeshPolygon> allFaces = new ArrayList<>();

		for(IMesh mesh : meshes) {
			allEdges.addAll( Arrays.asList(mesh.getEdges()) );
			allFaces.addAll( Arrays.asList(mesh.getFaces()) );
		}
		set(
			allEdges.toArray(new IMeshLine[allEdges.size()]),
			allFaces.toArray(new IMeshPolygon[allFaces.size()])
		);
	}

	/**
	 * Gets the edges.
	 * @return The edges.
	 */
	public IMeshLine[] getEdges() {
		int n = m_edges.length;
		IMeshLine[] edges = new MeshLine[n];
		for(int i=0; i<n; i++) edges[i] = new MeshLine(m_edges[i]);
		return edges;
	}

	/**
	 * Gets the faces.
	 * @return The faces.
	 */
	public IMeshPolygon[] getFaces() {
		int n = m_faces.length;
		IMeshPolygon[] faces = new MeshPolygon[n];
		for(int i=0; i<n; i++) faces[i] = new MeshPolygon(m_faces[i]);
		return faces;
	}

	/**
	 * Gets the face colours.
	 * @return The face colours.
	 */
	public Color[] getFaceColours() {
		int n = m_faces.length;
		Color[] colours = new Color[n];
		for(int i=0; i<n; i++) {
			colours[i] = deepCopy(m_faces[i].getColor());
		}
		return colours;
	}

	/**
	 * Gets the centre.
	 * @return The centre.
	 */
	public Point3d getCentre() {
		return m_centre==null ? null : new Point3d(m_centre);
	}

	/**
	 * Applies a 4x4 affine transformation.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	public void transform(Matrix4d matrix) {

		for(int i=0; i<m_edges.length; i++) {
			IMeshLine edge = m_edges[i];
			edge.transform(matrix);
			m_edges[i] = edge;
		}

		for(int i=0; i<m_faces.length; i++) {
			IMeshPolygon face = m_faces[i];
			face.transform(matrix);
			m_faces[i] = face;
		}

		if(m_centre!=null) matrix.transform(m_centre);
	}

	/**
	 * Checks if the object is equal to the mesh.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the mesh.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!IMesh.class.isAssignableFrom(obj.getClass())) return false;
		return equals((IMesh)obj);
	}

	/**
	 * Converts faces and face colours to mesh polygons.
	 * @param faces The faces.
	 * @param faceColours The face colours.
	 * @return The corresponding mesh polygons.
	 */
	private static IMeshPolygon[] toMeshPolygons(IPolygon3d[] faces, Color[] faceColours) {
		if(faces==null) return new IMeshPolygon[0];
		int n = faces.length;
		IMeshPolygon[] meshFaces = new IMeshPolygon[n];
		for(int i=0; i<n; i++) {
			meshFaces[i] = (faceColours == null)
				? new MeshPolygon(faces[i].getVertices())
				: new MeshPolygon(faces[i].getVertices(), faceColours[i]);
		}
		return meshFaces;
	}

	/**
	 * Converts edges to mesh lines.
	 * @param edges The edges.
	 * @return The corresponding mesh lines.
	 */
	private static IMeshLine[] toMeshLines(ILine3d[] edges) {
		if(edges==null) return new IMeshLine[0];
		int n = edges.length;
		IMeshLine[] meshLines = new MeshLine[n];
		for(int i=0; i<n; i++) {
			meshLines[i] = new MeshLine(edges[i]);
		}
		return meshLines;
	}

	/**
	 * Sets the mesh lines and mesh faces.
	 * @param edges The mesh lines.
	 * @param faces The mesh polygons.
	 */
	private void set(IMeshLine[] edges, IMeshPolygon[] faces) {
		m_edges = deepCopy(edges);

		validatePolygons(faces);
		m_faces = deepCopy(faces);

		m_centre = CentreFinder.getCentre(this);
	}

	/**
	 * Creates a deep copy of a colour.
	 * @param c The colour to copy.
	 * @return A deep copy of the colour.
	 */
	private Color deepCopy(Color c) {
		if(c == null) return null;
		return new Color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
	}

	/**
	 * Makes a copy of the edges.
	 * @param edges The edges to copy.
	 * @return The copied edges.
	 */
	private IMeshLine[] deepCopy(IMeshLine[] edges) {
		if(edges==null) return new MeshLine[0];
		int n = edges.length;
		IMeshLine[] copy = new MeshLine[n];
		for(int i=0; i<n; i++) copy[i] = new MeshLine(edges[i]);
		return copy;
	}

	/**
	 * Checks whether the polygons all have at least 3 vertices.
	 * @param polygons The polygons to check.
	 * @throws RuntimeException if some polygons are invalid.
	 */
	private void validatePolygons(IPolygon3d[] polygons) {
		if(polygons==null) return;
		for(IPolygon3d polygon : polygons) {
			if(polygon.size() < 3) throw new RuntimeException("Polygons must have at least 3 vertices.");
		}
	}

	/**
	 * Makes a copy of the polygons.
	 * @param polygons The polygons to copy.
	 * @return The copied polygons.
	 */
	private IMeshPolygon[] deepCopy(IMeshPolygon[] polygons) {
		if(polygons==null) return new IMeshPolygon[0];
		int n = polygons.length;
		IMeshPolygon[] copy = new MeshPolygon[n];
		for(int i=0; i<n; i++) copy[i] = new MeshPolygon(polygons[i]);
		return copy;
	}

	/**
	 * Checks for equality between two meshes.
	 * @param mesh The mesh with which to compare.
	 * @return Whether the meshes are equal.
	 */
	private boolean equals(IMesh mesh) {

		IMeshLine[] edges = mesh.getEdges();
		if (m_edges.length != edges.length) return false;
		for (int i = 0; i < m_edges.length; i++) {
			if(!m_edges[i].equals(edges[i])) return false;
		}

		IPolygon3d[] faces = mesh.getFaces();
		if(m_faces.length != faces.length) return false;
		for (int i = 0; i < m_faces.length; i++) {
			if (!m_faces[i].equals(faces[i])) return false;
		}

		return true;
	}

	/**
	 * A helper class for finding the centre of a mesh object.
	 */
	private static class CentreFinder {

		/**
		 * Computes the centre of a mesh object.
		 * @param mesh The mesh object.
		 * @return The centre of the mesh object.
		 */
		private static Point3d getCentre(IMesh mesh) {

			List<Point3d> points = new ArrayList<>();

			for(ILine3d edge : mesh.getEdges()) {
				points.add(edge.getP1());
				points.add(edge.getP2());
			}

			for(IPolygon3d face : mesh.getFaces()) {
				points.addAll(Arrays.asList(face.getVertices()));
			}

			return getCentre(points);
		}

		/**
		 * Computes the centre of a set of points.
		 * @param points The set of points.
		 * @return The centre of a set of points.
		 */
		private static Point3d getCentre(List<Point3d> points) {

			if(points==null || points.size()==0) return null;

			double minX = points.get(0).x;
			double minY = points.get(0).y;
			double minZ = points.get(0).z;
			double maxX = points.get(0).x;
			double maxY = points.get(0).y;
			double maxZ = points.get(0).z;

			for(Point3d p : points) {
				minX = Math.min(minX, p.x);
				minY = Math.min(minY, p.y);
				minZ = Math.min(minZ, p.z);
				maxX = Math.max(maxX, p.x);
				maxY = Math.max(maxY, p.y);
				maxZ = Math.max(maxZ, p.z);
			}

			return new Point3d((minX+maxX)/2.0, (minY+maxY)/2.0, (minZ+maxZ)/2.0);
		}
	}
}
