package Mesh;

import Geometry.Lines.ILine3d;
import Geometry.Lines.Line3d;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;

/**
 * Lines used in the Mesh object.
 */
public class MeshLine extends Line3d implements IMeshLine {

	/**
	 * Outward normal vectors associated with the line.
	 */
	private List<Vector3d> m_normals; // don't use set since duplicates are allowed

	/**
	 * Whether the normals are fixed or subject to transformations.
	 */
	private boolean m_fixed;

	/**
	 * Constructor with endpoint arguments. No normal vectors set.
	 * @param p1 First endpoint.
	 * @param p2 Second endpoint.
	 */
	public MeshLine(Point3d p1, Point3d p2) {
		super(p1, p2);
		m_normals = new ArrayList<>();
		m_fixed = false;
	}

	/**
	 * Constructor with line argument. No normal vectors set.
	 * @param line The line.
	 */
	public MeshLine(ILine3d line) {
		super(line);
		m_normals = new ArrayList<>();
		m_fixed = false;
	}

	/**
	 * Constructor from endpoints, normal vectors, and fixed value.
	 * @param p1 First endpoint.
	 * @param p2 Second endpoint.
	 * @param normals Normal vectors of the line.
	 * @param fixed Whether normals are fixed or subject to transformations.
	 */
	public MeshLine(Point3d p1, Point3d p2, List<Vector3d> normals, boolean fixed) {
		super(p1, p2);
		m_normals = new ArrayList<>();
		addNormals(normals);
		m_fixed = fixed;
	}

	/**
	 * Copy constructor.
	 * @param line The line to copy from.
	 */
	public MeshLine(IMeshLine line) {
		super(line);
		m_normals = new ArrayList<>();
		addNormals(line.getNormals());
		m_fixed = line.isFixed();
	}

	/**
	 * Gets the outward normal vectors of the line.
	 * @return outward normal vectors.
	 */
	public List<Vector3d> getNormals() {
		List<Vector3d> copy = new ArrayList<>();
		for(Vector3d normal : m_normals) {
			copy.add(new Vector3d(normal));
		}
		return copy;
	}

	/**
	 * Whether the normals are fixed or subject to transformations.
	 * @return Whether normals are fixed.
	 */
	public boolean isFixed() {
		return m_fixed;
	}

	/**
	 * Applies a 4x4 affine transformation.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	public void transform(Matrix4d matrix) {

		matrix.transform(m_p1);
		matrix.transform(m_p2);

		if(!m_fixed) {
			for(int i=0; i<m_normals.size(); i++) {
				Vector3d normal = m_normals.get(i);
				matrix.transform(normal);
				m_normals.set(i, normal);
			}
		}
	}

	/**
	 * Checks if the object is equal to the line.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the line.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!IMeshLine.class.isAssignableFrom(obj.getClass())) return false;
		return equals((IMeshLine) obj);
	}

	/**
	 * Checks if the two lines are equal.
	 * @param line The line with which to compare.
	 * @return Whether the lines are equal.
	 */
	private boolean equals(IMeshLine line) {
		if(!super.equals(line)) return false;

		List<Vector3d> normals = line.getNormals();
		int numNormals = m_normals.size();
		if(numNormals != normals.size()) return false;
		for(int i=0; i<numNormals; i++) {
			if(!m_normals.get(i).equals(normals.get(i))) return false;
		}

		if(m_fixed != line.isFixed()) return false;

		return true;
	}

	/**
	 * Adds a list of normal vectors.
	 * @param normals Normal vectors to add.
	 */
	private void addNormals(List<Vector3d> normals) {
		addNormals(normals.toArray(new Vector3d[normals.size()]));
	}

	/**
	 * Adds an array of normal vectors.
	 * @param normals Normal vectors to add.
	 */
	private void addNormals(Vector3d... normals) {
		for(Vector3d normal : normals) {
			if(normal != null) m_normals.add(normal);
		}
	}
}
