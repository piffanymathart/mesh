package Mesh;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import java.awt.*;


/**
 * A set of edges and faces representing a 3D object.
 */
public interface IMesh {

	/**
	 * Gets the edges.
	 * @return The edges.
	 */
	IMeshLine[] getEdges();

	/**
	 * Gets the faces.
	 * @return The faces.
	 */
	IMeshPolygon[] getFaces();

	/**
	 * Gets the face colours.
	 * @return The face colours.
	 */
	Color[] getFaceColours();

	/**
	 * Gets the centre.
	 * @return The centre.
	 */
	Point3d getCentre();

	/**
	 * Applies a 4x4 affine transformation.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	void transform(Matrix4d matrix);

	/**
	 * Checks if the object is equal to the mesh.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the mesh.
	 */
	@Override
	boolean equals(Object obj);

}
